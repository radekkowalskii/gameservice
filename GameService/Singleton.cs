﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService
{
    /*
    * sealed - uniemożliwia dziedziczenie po klasie Singleton
    */
    public sealed class Singleton
    {
        public static Singleton game;
        public int id { get; set; }
        public List<string> players { get; set; }

        private Singleton() { }

        public static Singleton CreateInstance()
        {
            if (game == null)
            {
                game = new Singleton();
            }
            return game;
        }
    }
}
