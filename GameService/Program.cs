﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Timers;

namespace GameService
{
    class Program
    {
        static void Main(string[] args)
        {
            #region test funkcji random
            //Random r = new Random();
            //int randomInteger = r.Next(16, 25);
            //Console.WriteLine(randomInteger);
            //Console.ReadKey(); 
            #endregion

            
            #region Test
            //Message m = new Message(1, "możesz synchronizować");
            //var x = Message.Serialize(1, "Zsynchronizuj dane!"); 

            //#region serializacja
            //Message m = new Message(1, "Synchronizacja!");
            //var x = m.SerializeToJson();
            //Message mesObject = Message.DeserializeToObject(x);
            //Console.WriteLine(x);
            //Console.WriteLine(mesObject.Text);
            //Console.Read();
            //#endregion


            //TcpListener listener = new TcpListener(localAddress, port);
            //Console.WriteLine("Listening");
            //listener.Start();

            /*CLIENT*/
            //TcpClient client = listener.AcceptTcpClient();

            /*GETTING INCOMING DATA*/

            //byte[] buffer = new byte[client.ReceiveBufferSize];

            ///*READING INCOMING DATA*/
            //int readBytes = dataStream.Read(buffer, 0, client.ReceiveBufferSize);

            ///*CONVERTING RECEIVED DATA INTO STRING*/
            //string receivedData = Encoding.ASCII.GetString(buffer, 0, readBytes);
            //Console.WriteLine("Received: " + receivedData); 
            #endregion

            #region serwer
            //const int port = 5000;
            //const string ip = "127.0.2.1";

            //IPAddress localAddress = IPAddress.Parse(ip);
            //TcpClient client = new TcpClient(ip, port);
            //NetworkStream dataStream = client.GetStream();
            ///*SENDING DATA TO CLIENT*/
            //Console.WriteLine("Sending data to client: Odpowiedź serwera");
            //byte[] serverBuffer = ASCIIEncoding.UTF8.GetBytes("Odpowiedź serwera");
            //dataStream.Write(serverBuffer, 0, serverBuffer.Length);

            ///*CLIENT AND LISTENER CLOSING*/
            //client.Close();
            ////listener.Stop();
            //Console.ReadLine();
            #endregion

            #region Timer
            //Timer t = new Timer();
            //t.Elapsed += T_Elapsed;
            //t.Interval = 5000;
            //t.Enabled = true;

            //Timer t2 = new Timer(3000);
            //t2.Elapsed += T2_Elapsed;
            ////t2.Interval = 10000;
            //t2.Enabled = true;


            //Console.WriteLine("Press \'q\' to quit");
            //while (Console.Read()!='q')
            //{

            //}
            #endregion

            #region testy funkcji
            //Singleton game = Singleton.CreateInstance();
            //game.id = Tools.GetAppropriateGameID();
            //var ifAreEnoughUsers = Tools.CheckAvailableUsers();
            //if (ifAreEnoughUsers)
            //{
            //    game.players = Tools.GetAvailablePlayers();
            //    Tools.AgregateAvailablePlayers(game.players, game.id);
            //    Tools.SetPlayersColors(game.players);

            //    Rounds.BeginGame(game.players);
            //}
            //var y = Int32.Parse(game.players[2]);
            //Tools.NotifyUser(Int32.Parse(game.players[2]), Tools.NotificationType.BeginFirstRound);
            //Tools.NotifyUser(Int32.Parse(game.players[1]), Tools.NotificationType.BeginSynchronization);
            //Tools.NotifyUser(Int32.Parse(game.players[0]), Tools.NotificationType.BeginSynchronization);


            //Timer t = new Timer();
            //t.Elapsed += T_Elapsed;
            ////t.Elapsed += (sender, e) => MyElapsedMethod(sender, e, game);
            //t.Interval = 5000;
            //t.Enabled = true;
            //System.Threading.Thread.Sleep(4000);

            //Console.WriteLine("Press \'q\' to quit");
            //while (Console.Read() != 'q') ;
            //Boolean x = false;
            //x = Tools.CheckAvailableUsers();
            //int x = Tools.GetAppropriateGameID();
            //Tools.CheckAvailableUsers();
            //Tools.AgregateAvailablePlayers();
            //Console.ReadKey(); 
            #endregion
            #region Game
            Tools.SendIPAddressToDB();
            while (true)
            {
                var ifAreEnoughUsers = Tools.CheckAvailableUsers();
                if (ifAreEnoughUsers)
                {
                    Singleton game = Singleton.CreateInstance();
                    game.id = Tools.GetAppropriateGameID();

                    game.players = Tools.GetAvailablePlayers();
                    Tools.AgregateAvailablePlayers(game.players, game.id);
                    Tools.SetPlayersColors(game.players);

                    Rounds rounds = new Rounds();
                    rounds.BeginGame(game.players, game.id);
                }


                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5));
            } 
            #endregion
        }

        static void MyElapsedMethod(object sender, ElapsedEventArgs e, string theString)
        {

        }

        private  static void T2_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("minęły 3 sekundy\n");
        
            
        }

        private static void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("minęło 5 sekund\n");
            
        }
        
    }
}
