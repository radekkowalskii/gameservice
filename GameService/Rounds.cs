﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService
{
    class Rounds
    {
        int game_id = 0;
        public void Begin1Round(List<string> players)
        {
            int PlayerId1 = Int32.Parse(players.ElementAt(0));
            int PlayerId2 = Int32.Parse(players.ElementAt(1));
            int PlayerId3 = Int32.Parse(players.ElementAt(2));
            //foreach (string player_id in players)
            //{
            //    Tools.NotifyUser(Int32.Parse(player_id), Tools.NotificationType.BeginFirstRound);
            //}
            /*Przesłanie numeru rundy 1*/
            Tools.NotifyUsers(players, Tools.NotificationType.Round1Starts, game_id);

            /*Poinformowanie graczy o rozpoczęciu rozgrywki*/
            Tools.NotifyUsers(players, Tools.NotificationType.GameStartsInfo, game_id);

            /*Ruchy pierwszego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId1, true);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            //System.Threading.Thread.Sleep(120000);
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId1, false);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.RoundTimeElapsed);

            
            /*Ruchy drugiego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId2, true);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId2, false);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.RoundTimeElapsed);

            
            /*Ruchy trzeciego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId3, true);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId3, false);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.RoundTimeElapsed);

            /*Wypłacenie pieniędzy graczom*/
            Tools.PayPlayers(players);

            /*Powiadomienie o możliwości synchronizacji*/
            Tools.NotifyUsers(players, Tools.NotificationType.BeginSynchronization, game_id);
        }

        public void Begin2Round(List<string> players)
        {
            int PlayerId1 = Int32.Parse(players.ElementAt(0));
            int PlayerId2 = Int32.Parse(players.ElementAt(1));
            int PlayerId3 = Int32.Parse(players.ElementAt(2));

            /*Przesłanie numeru rundy 2*/
            Tools.NotifyUsers(players, Tools.NotificationType.Round2Starts, game_id);

            /*Zagranie karty regulacji odnośnie wskaźnika typ=1 poziom=1*/
            Tools.NotifyUsers(players, Tools.NotificationType.RegulationCardLevel1, game_id);

            /*Ruchy pierwszego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId1, true);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId1, false);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy drugiego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId2, true);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId2, false);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy trzeciego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId3, true);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId3, false);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.RoundTimeElapsed);

            /*Wypłacenie pieniędzy graczom*/
            Tools.PayPlayers(players);

            /*Powiadomienie o możliwości synchronizacji*/
            Tools.NotifyUsers(players, Tools.NotificationType.BeginSynchronization, game_id);
        }

        public void Begin3Round(List<string> players)
        {
            int PlayerId1 = Int32.Parse(players.ElementAt(0));
            int PlayerId2 = Int32.Parse(players.ElementAt(1));
            int PlayerId3 = Int32.Parse(players.ElementAt(2));

            /*Przesłanie numeru rundy 3*/
            Tools.NotifyUsers(players, Tools.NotificationType.Round3Starts, game_id);

            /*Zagranie karty regulacji odnośnie wskaźnika typ=2 poziom=2*/
            Tools.NotifyUsers(players, Tools.NotificationType.RegulationCardLevel2, game_id);

            /*Ruchy pierwszego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId1, true);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId1, false);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy drugiego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId2, true);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId2, false);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy trzeciego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId3, true);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId3, false);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.RoundTimeElapsed);

            /*Wypłacenie pieniędzy graczom*/
            Tools.PayPlayers(players);

            /*Powiadomienie o możliwości synchronizacji*/
            Tools.NotifyUsers(players, Tools.NotificationType.BeginSynchronization, game_id);
        }

        public void Begin4Round(List<string> players)
        {
            int PlayerId1 = Int32.Parse(players.ElementAt(0));
            int PlayerId2 = Int32.Parse(players.ElementAt(1));
            int PlayerId3 = Int32.Parse(players.ElementAt(2));

            /*Przesłanie numeru rundy 4*/
            Tools.NotifyUsers(players, Tools.NotificationType.Round4Starts, game_id);

            /*Zagranie karty regulacji odnośnie wskaźnika typ=1 poziom=3*/
            Tools.NotifyUsers(players, Tools.NotificationType.RegulationCardLevel3, game_id);

            /*Ruchy pierwszego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId1, true);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId1, false);
            Tools.NotifyUser(PlayerId1, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy drugiego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId2, true);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId2, false);
            Tools.NotifyUser(PlayerId2, Tools.NotificationType.RoundTimeElapsed);

            /*Ruchy trzeciego gracza*/
            Tools.EnableOrDisablePlayerTurn(PlayerId3, true);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.YourTurn);
            //przydzielenie graczowi 2 minut na wykonanie ruchów
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Tools.EnableOrDisablePlayerTurn(PlayerId3, false);
            Tools.NotifyUser(PlayerId3, Tools.NotificationType.RoundTimeElapsed);

            /*Wypłacenie pieniędzy graczom*/
            Tools.PayPlayers(players);

            /*Powiadomienie o możliwości synchronizacji*/
            Tools.NotifyUsers(players, Tools.NotificationType.BeginSynchronization, game_id);

            /*Końcowa informacja (ogłoszenie zwycięzcy)*/
            Tools.NotifyUsers(players, Tools.NotificationType.GameTermination, game_id);
        }

        public void BeginGame(List<string> players, int gameId)
        {
            game_id = gameId;
            Begin1Round(players);
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Begin2Round(players);
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Begin3Round(players);
            System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            Begin4Round(players);
        }
    }
}
