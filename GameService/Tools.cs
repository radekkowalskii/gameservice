﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Linq;

namespace GameService
{
    public class Tools
    {
        public enum NotificationType { YourTurn, BeginSynchronization, RoundTimeElapsed, BeginFirstRound, GameStartsInfo, RegulationCardLevel1,
                                       RegulationCardLevel2, RegulationCardLevel3, RegulationCardLevel4, Round1Starts, Round2Starts, Round3Starts, Round4Starts,
                                       GameTermination};

        public static bool CheckAvailableUsers()
        {
            bool success = false;
                DataTable result = new DataTable();
                using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    conn.Open();
                    //conn.Notice += new NoticeEventHandler(myConnection_Notice);
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT id FROM users WHERE ingame <> TRUE ORDER BY id ASC";

                        using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(cmd))
                        {
                            dataAdapter.Fill(result);
                        }
                    }
                }
            if (result.Rows.Count >= 3)
            {
                return success = true;
            }
            else
                return success = false;
        }

        public static List<string> GetAvailablePlayers()
        {
            List<string> activePlayers = new List<string>();
            DataTable result = new DataTable();
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT id FROM users WHERE ingame <> TRUE AND game_id IS NULL ORDER BY id ASC";

                    using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(cmd))
                    {
                        dataAdapter.Fill(result);
                    }
                }
            }
            if (result.Rows.Count >= 3)
            {
                for (int i = 0; i <= 2; i++)
                {
                    activePlayers.Add(result.Rows[i][0].ToString());
                }
                return activePlayers;
            }
            else
                return null;
        }

        public static void AgregateAvailablePlayers(List<string> players, int game_id)
        {
            var gameID = game_id;
            foreach (var player_id in players)
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.Parameters.AddWithValue("game_id", gameID);
                        cmd.Parameters.AddWithValue("id", Int32.Parse(player_id));
                        cmd.Parameters.AddWithValue("ingame", true);
                        cmd.CommandText = "UPDATE users SET game_id = @game_id, ingame = @ingame WHERE id = @id; ";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public static int GetAppropriateGameID()
        {
            Int32 maxGameID = 0;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT game_id FROM users WHERE game_id IS NOT NULL ORDER BY game_id DESC LIMIT 1 ";
                    //maxGameID = Int32.Parse(cmd.ExecuteScalar().ToString());
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                if (dr["game_id"] != DBNull.Value && dr["game_id"] != null)
                                {
                                    maxGameID = Int32.Parse(dr["game_id"].ToString());
                                }
                                else
                                    maxGameID = 0;
                            }
                        }
                    }
                }

                return maxGameID + 1;
            }
        }
        public static void SendIPAddressToDB()
        {
            IPHostEntry host;
            string IP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP = ip.ToString();
                }
            }
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("ip", IP);
                    cmd.Parameters.AddWithValue("id", 1);
                    cmd.CommandText = "UPDATE config SET text = @ip WHERE id = @id";
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public static string GetIpAddress()
        {
            IPHostEntry host;
            string IP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP = ip.ToString();
                }
            }
            return IP;
        }

        public static string GetPlayerColor(int player_id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = "SELECT color FROM users WHERE id = @id";
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }

        public static string GetUserIPAddress(int player_id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = "SELECT ip FROM users WHERE id = @id";
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }
        public static void NotifyUser(int player_id, NotificationType notifyType)
        {
            const int port = 5000;
            string ip = GetUserIPAddress(player_id);
            string message = string.Empty;

            IPAddress localAddress = IPAddress.Parse(ip);
            TcpClient client = new TcpClient(ip, port);
            NetworkStream dataStream = client.GetStream();

            Console.WriteLine("Sending data to client: Odpowiedź serwera");
            
            switch (notifyType)
            {
                case NotificationType.YourTurn:
                    Message m = new Message();
                    m.Text = "Twój ruch! Masz 2 minuty na dokonanie zmian!";
                    message = m.SerializeToJson();
                    break;
                case NotificationType.BeginSynchronization:
                    Message m1 = new Message();
                    m1.Text = "Rozpocznij synchronizację!";
                    message = m1.SerializeToJson();
                    break;
                case NotificationType.RoundTimeElapsed:
                    Message m2 = new Message();
                    m2.Text = "Czas Twojego ruchu upłynął!";
                    message = m2.SerializeToJson();
                    break;
                default:
                    break;
            }
            byte[] serverBuffer = ASCIIEncoding.UTF8.GetBytes(message);
            dataStream.Write(serverBuffer, 0, serverBuffer.Length);

            client.Close();
            //Console.ReadLine();
        }

        public static void NotifyUsers(List<string> playersIDs, NotificationType notifyType, int game_id)
        {
            
            const int port = 5000;
            
            //Console.WriteLine("Sending data to client: Odpowiedź serwera");
            
            foreach (var id in playersIDs)
            {
                int player_id = Int32.Parse(id);
                string message = string.Empty;

                switch (notifyType)
                {
                    case NotificationType.BeginSynchronization:
                        Message mSynchro = new Message();
                        mSynchro.Type = 3;
                        mSynchro.Text = "Rozpocznij synchronizację!";
                        message = mSynchro.SerializeToJson();
                        break;
                    case NotificationType.GameStartsInfo:
                        Message m = new Message(1, "Gra rozpoczęta, oczekuj na swój ruch!", GetPlayerColor(player_id), game_id);
                        message = m.SerializeToJson();
                        break;
                    case NotificationType.Round1Starts:
                        Message mR1 = new Message(2, "","", 1);
                        message = mR1.SerializeToJson();
                        break;
                    case NotificationType.Round2Starts:
                        Message mR2 = new Message(2, "", "", 2);
                        message = mR2.SerializeToJson();
                        break;
                    case NotificationType.Round3Starts:
                        Message mR3 = new Message(2, "", "", 3);
                        message = mR3.SerializeToJson();
                        break;
                    case NotificationType.Round4Starts:
                        Message mR4 = new Message(2, "", "", 4);
                        message = mR4.SerializeToJson();
                        break;
                    case NotificationType.RegulationCardLevel1:
                        var cardInfo1 = PlayRegulationCard(1, 1, player_id);
                        Message m1 = new Message(5, cardInfo1[0], "", player_id);
                        message = m1.SerializeToJson();
                        break;
                    case NotificationType.RegulationCardLevel2:
                        var cardInfo2 = PlayRegulationCard(2, 2, player_id);
                        Message m2 = new Message(5, cardInfo2[0], "", player_id);
                        message = m2.SerializeToJson();
                        break;
                    case NotificationType.RegulationCardLevel3:
                        var cardInfo3 = PlayRegulationCard(3, 1, player_id);
                        Message m3 = new Message(5, cardInfo3[0], "", player_id);
                        message = m3.SerializeToJson();
                        break;
                    case NotificationType.RegulationCardLevel4:
                        var cardInfo4 = PlayRegulationCard(4, 2, player_id);
                        Message m4 = new Message(5, cardInfo4[0], "", player_id);
                        message = m4.SerializeToJson();
                        break;
                    case NotificationType.GameTermination:
                        Message termination_message = new Message();
                        termination_message.Text = TerminateGame(game_id);
                        message = termination_message.SerializeToJson();
                        break;
                    default:
                    break;
                }

                string ip = GetUserIPAddress(player_id);

                IPAddress localAddress = IPAddress.Parse(ip);
                TcpClient client = new TcpClient(ip, port);
                NetworkStream dataStream = client.GetStream();

                byte[] serverBuffer = ASCIIEncoding.UTF8.GetBytes(message);
                dataStream.Write(serverBuffer, 0, serverBuffer.Length);

                client.Close(); 
            }
            //Console.ReadLine();
        }
        public static void SetPlayersColors(List<string> playersIDs)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                for (int i = 0; i < playersIDs.Count; i++)
                {
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        List<string> colors = new List<string> { "blue", "white", "yellow" };
                    
                            cmd.Parameters.AddWithValue("id", Int32.Parse(playersIDs.ElementAt(i)));
                            cmd.Parameters.AddWithValue("color", colors.ElementAt(i));
                            cmd.CommandText = "UPDATE users SET color = @color WHERE id = @id;";
                            cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        public static void EnableOrDisablePlayerTurn(int player_id, bool enable)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                {
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        if (enable)
                            cmd.Parameters.AddWithValue("canplay", true);
                        else
                            cmd.Parameters.AddWithValue("canplay", false);
                        cmd.Parameters.AddWithValue("id", player_id);
                        cmd.CommandText = "UPDATE users SET canplay = @canplay WHERE id = @id;";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        public static Dictionary<int,string> PlayRegulationCard(int indicator_level, int type, int player_id)
        {
            Dictionary<int, string> cardInfo = new Dictionary<int, string>();

            int indicators_ok = 0;
            int indicators_not_ok = 0;
            int result = 0;
            string indicator_type = string.Empty;
            string message = string.Empty;

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                {
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.Parameters.AddWithValue("indicator_level", indicator_level);
                        cmd.Parameters.AddWithValue("active", true);
                        cmd.Parameters.AddWithValue("type", type);  
                        cmd.CommandText = "SELECT message, indicator_type FROM card WHERE type::integer = @type AND active = @active AND indicator_level = @indicator_level";
                        using (NpgsqlDataReader dr = cmd.ExecuteReader())
                        {
                            message = dr["message"].ToString();
                            indicator_type = dr["indicator_type"].ToString();
                            
                        }
                       

                        //sprawdzanie ile nadajników spełnia kartę regulacji i przyznawanie nagrody pieniężnej
                        cmd.Parameters.AddWithValue("player_id",player_id);
                        cmd.Parameters.AddWithValue("level", indicator_level);
                        cmd.CommandText = "SELECT COUNT(*) FROM indicator WHERE player_id = @player_id AND level >= @level";
                        Int32.TryParse(cmd.ExecuteScalar().ToString(), out indicators_ok);
                        if (indicators_ok > 0)
                        {
                            Random r = new Random();
                            //losowa liczba id karty nagrody
                            int randomInteger = r.Next(16, 25);

                            cmd.Parameters.AddWithValue("id", randomInteger);
                            cmd.CommandText = "SELECT result FROM card WHERE id = @id";
                            Int32.TryParse(cmd.ExecuteScalar().ToString(), out result);

                            cardInfo.Add(0,String.Format("{0} Otrzymujesz {1}.", message, result));
                            cardInfo.Add(1, indicator_type.ToString());

                            //aktualizacja salda konta (cash) gracza
                            PayPlayer(player_id, result);

                            return cardInfo;
                        }
                        //sprawdzanie ile nadajników nie spełnia karty regulacji i przyznawanie kary pieniężnej
                        cmd.Parameters.AddWithValue("player_id", player_id);
                        cmd.Parameters.AddWithValue("level", indicator_level);
                        cmd.CommandText = "SELECT COUNT(*) FROM indicator WHERE player_id = @player_id AND level < @level";
                        Int32.TryParse(cmd.ExecuteScalar().ToString(), out indicators_ok);
                        if (indicators_not_ok > 0)
                        {
                            Random r = new Random();
                            //losowa liczba id karty nagrody
                            int randomInteger = r.Next(29, 33);

                            cmd.Parameters.AddWithValue("id", randomInteger);
                            cmd.CommandText = "SELECT result FROM card WHERE id = @id";
                            Int32.TryParse(cmd.ExecuteScalar().ToString(), out result);

                            cardInfo.Add(0, String.Format("{0} Oddajesz do banku {1}.", message, result));
                            cardInfo.Add(1, indicator_type.ToString());

                            //aktualizacja salda konta (cash) gracza
                            PayPlayer(player_id, -result);

                            return cardInfo;
                        }
                        else
                        {
                            //jeśli gracz ma 0 wskaźników, czyli nie ma żadnego nadajnika
                            
                                cardInfo.Add(0, String.Format("{0} Nie posiadasz nadajników.", message));
                                cardInfo.Add(1, indicator_type.ToString());
                                return cardInfo;
                            
                        }
                    }
                }
            }
            
        }

        public static void PayPlayers(List<string> playersIDs)
        {
            int current_cash = 0;
            int clients_count = 0;
            int price_per_client = GetPricePerClient();
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        foreach (var player_id in playersIDs)
                        {
                            cmd.Parameters.AddWithValue("id", Int32.Parse(player_id));
                            cmd.CommandText = "SELECT cash, clients_number FROM users WHERE id = @id";
                        using (NpgsqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    if (dr["cash"] != DBNull.Value || dr["cash"] != null)
                                    {
                                        Int32.TryParse(dr["cash"].ToString(), out current_cash); 
                                    }
                                    else 
                                        current_cash = 0;

                                    if (dr["clients_number"] != DBNull.Value || dr["clients_number"] != null)
                                    {
                                        Int32.TryParse(dr["clients_number"].ToString(), out clients_count);
                                    }
                                    else
                                        clients_count = 0;
                                }
                            }
                        }  

                            cmd.Parameters.AddWithValue("id", player_id);
                            cmd.Parameters.AddWithValue("cash", clients_count != 0 ? (current_cash + (clients_count * price_per_client)) : current_cash);
                            cmd.CommandText = "UPDATE users SET cash = @cash WHERE id = @id;";
                            cmd.ExecuteNonQuery(); 
                        }
                    }
                
            }
        }
        public static void PayPlayer(int player_id, int how_much)
        {
            int current_cash = 0;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                        cmd.Parameters.AddWithValue("id", player_id);
                        cmd.CommandText = "SELECT cash FROM users WHERE id = @id";

                        Int32.TryParse(cmd.ExecuteScalar().ToString(), out current_cash);
                        
                        cmd.Parameters.AddWithValue("id", player_id);
                        cmd.Parameters.AddWithValue("cash", current_cash + how_much);
                        cmd.CommandText = "UPDATE users SET cash = @cash WHERE id = @id;";
                        cmd.ExecuteNonQuery();
                    
                }

            }
        }

        public static int GetPricePerClient()
        {
            int price = 0;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", 1);
                    cmd.CommandText = "SELECT value_int from setup where id = @id";
                    Int32.TryParse(cmd.ExecuteScalar().ToString(), out price);
                }

            }
            return price;
        }

        public static string TerminateGame(int game_id)
        {
            string winner_name = string.Empty;
            string winner_cash = string.Empty;
            string winner_clients = string.Empty;

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("game_id", game_id);
                    cmd.CommandText = "SELECT username, cash, clients_number FROM users WHERE game_id = @game_id ORDER BY cash DESC LIMIT 1";
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                winner_name = dr["username"].ToString();
                                winner_cash = dr["cash"].ToString();
                                winner_clients = dr["clients_number"].ToString();
                            }
                        }
                    }
                }
            }
            return string.Format("Zwyciężył {0} zdobywając {1} monet oraz {2} klientów!", winner_name, winner_cash, winner_clients);
        }

    }
}
